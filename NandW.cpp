//
//Justin Kincaid <JKincaid@nau.edu>
//Needleman and Wunsch algorithm
//uses a struct to store alignment results
//can be used to do Smith and Waterman Algorithm for alignment
//set doSandW to true to do a Smith and Waterman alignment
//
//

#include "NandW.h"


//constructor
//Needleman and Wunsch algortihm
//default value of doSandW is false
//when the object is created
//use doS_W to change its state to do
NandW::NandW() {

    this->doSandW = false;

}
//set true to perform Smith and Waterman alignment
//else false for Needleman and Wunsch
//default value is false;
void NandW::doS_W(bool doSmithWaterman){

    this->doSandW = doSmithWaterman;


}

//Do a Needleman and Wunsch alignment
//@param simScoreMatrix =  2x2 int scoring matrix as a parameter
//@param gapScore = int gap penalty
//stores the results in a results struct member labeled 'current'
int NandW::NandWgetAlign(int **simScoreMatrix, int gapScore) {

    this->scoreAndTracebackInit(gapScore);

    for(int i = 1; i <= querySize; i++){


        for(int j = 1; j <= subjectSize; j++){

            simScoreComp = current.subject[j - 1];


            switch( simScoreComp )
            {
                case 'A':  simScoreMatrix_x = 0 ;  break ;
                case 'C':  simScoreMatrix_x = 1 ;  break ;
                case 'G':  simScoreMatrix_x = 2 ;  break ;
                case 'T':  simScoreMatrix_x = 3 ;
            }

            simScoreComp = current.query[i - 1 ] ;

            switch( simScoreComp )
            {
                case 'A':  simScoreMatrix_y = 0 ;  break ;
                case 'C':  simScoreMatrix_y = 1 ;  break ;
                case 'G':  simScoreMatrix_y = 2 ;  break ;
                case 'T':  simScoreMatrix_y = 3 ;
            }

            up = scoreMatrix[i-1][j] + gapScore;
            diagnal = scoreMatrix[i-1][j-1] + simScoreMatrix[simScoreMatrix_x][simScoreMatrix_y];
            left = scoreMatrix[i][j-1] + gapScore;

            scoreMatrix[i][j]= max(up,diagnal,left, &tbChar);


            tracebackMatrix[i][j] = tbChar;

        }
    }

    //printMatrix(0);
    //printMatrix(1);

    int i = querySize;
    int j = subjectSize;

    alignScore = 0;
    int tmpGapQuery = 0;
    int tmpGapSubject = 0;
    int tmpGapScore=0;

    current.alignSubject.clear();
    current.alignQuery.clear();
    current.traceback.clear();


    int lastIndexOfQuery=0;

    while( i >0 || j >0){

        switch(tracebackMatrix[i][j]){

            case UP :
                tmpGapQuery++;
                current.alignSubject.insert(0, 1, '_');
                current.traceback.insert(0,1,GAP);
                current.alignQuery.insert(0, 1, current.query[i - 1]);
                i--;
                break;

            case DIAG :

                if(tmpGapQuery>=tmpGapSubject){
                    tmpGapScore = tmpGapQuery;

                }else
                    tmpGapScore = tmpGapSubject;

                if(tmpGapScore > alignScore)
                    alignScore = tmpGapScore;

                tmpGapQuery =  0;
                tmpGapScore = 0;
                tmpGapSubject =0;

                if(current.query[i - 1] == current.subject[j - 1]) {
                    current.traceback.insert(0,1,MATCH);

                }

                else
                    current.traceback.insert(0,1,MISMATCH);

                current.alignSubject.insert(0, 1, current.subject[j - 1]);
                current.alignQuery.insert(0, 1, current.query[i - 1]);

                i--;
                j--;
                break;

            case LEFT:
                tmpGapSubject++;
                current.alignSubject.insert(0, 1, current.subject[j - 1]);
                current.alignQuery.insert(0, 1, '_');
                current.traceback.insert(0,1,GAP);
                j--;
                break;


            case STOP:

                    lastIndexOfQuery = j;
                    j = 0;
                    i = 0;

                    break;


            default:
                if(i>0)
                    --i;
                else
                    j--;
                break;

        };

    };
        //pretty print the strings for SandW
        //helps visualize where on the string was the local alignment
        if(lastIndexOfQuery>0){

            for(int tq = lastIndexOfQuery-1; tq >=0; --tq){
                current.alignSubject.insert(0, 1, current.subject[tq]);
            }
            for(int tq = lastIndexOfQuery-1; tq >=0; --tq) {
                current.alignQuery.insert(0, 1, ' ');

            }
            for(int tq = lastIndexOfQuery-1; tq >=0; --tq){
                current.traceback.insert(0,1,' ');
            }
        }




    for(int i = 0; i <= querySize; i ++) {
        delete scoreMatrix[i];
        delete tracebackMatrix[i];
    }

    delete[] scoreMatrix;
    delete[] tracebackMatrix;

    current.score = alignScore * -1;

    return 0;

}
//initialize the score matrix
//MUST BE DONE BEFORE NandWgetAlign
//called in NandWgetAlign, could be called outside for large static query size and subject size alignments
//and the trace back matrix
//check if doing smith and waterman to change initialization edges of traceback  matrix
//takes gap penalty as parameter
void NandW::scoreAndTracebackInit(int gap) {

    int setGap = gap;

    if(doSandW){
        setGap = 0;
    }

    scoreMatrix = new int * [querySize + 1];
    for(int i =0; i <= querySize; i++)
        scoreMatrix[i] = new int [subjectSize + 1];

    tracebackMatrix = new char* [querySize + 1];
    for(int i =0; i <= querySize; i++)
        tracebackMatrix[i] = new char [subjectSize + 1];

    scoreMatrix[0][0] = 0;


    tracebackMatrix[0][0] = STOP;


    for(int i = 1; i <= querySize; i++){

        scoreMatrix[i][0] = i*setGap;
        if(doSandW)
            tracebackMatrix[i][0] = STOP;
        else
            tracebackMatrix[i][0] = UP;

        //printf("%d", scoreMatrix[i][0]);
    }
    for(int j = 1; j <= subjectSize; j++){

        scoreMatrix[0][j] = j*setGap;
        if(doSandW)
            tracebackMatrix[0][j] = STOP;
        else
            tracebackMatrix[0][j] = LEFT;

    }



}
//Sets the subject to be used during th alignment algorithm
//MUST BE CALLED BEFORE NANDGETALIGN()
//@param subject = char* of nucleotide sequence
//@param length  = int length of the nucleotide sequence 'subject'
void NandW::setSubject(char *subject, int length) {

    current.subject = subject;

    subjectSize = length;




}
//Sets the query to be used during th alignment algorithm
//MUST BE CALLED BEFORE NANDGETALIGN()
//@param subject = char* of nucleotide sequence
//@param length  = int length of the nucleotide sequence 'subject'
void NandW::setQuery(char *query, int length) {


    current.query = query;
    querySize = length;



}

//returns the max of input parameters
//sets traceback character
//
int NandW::max(int Up, int Diag, int Left, char* tbChar) {

    tempMax = 0;
    if(!doSandW || (Up> 0 || Diag >0 || Left >0)) {

        if (Up >= Diag && Up >= Left) {

            tempMax = Up;
            *tbChar = UP;

        }
        else if (Diag > Left) {

            tempMax = Diag;
            *tbChar = DIAG;
        }
        else {

            tempMax = Left;
            *tbChar = LEFT;
        }
    }else{

        *tbChar = STOP;
    }

    return tempMax;
}

//return the results struct
//
//
NandW::results NandW::getResults() {

    return current;
}

void NandW::clearResults(){

}


void NandW::printMatrix(bool isTraceback){

    if(isTraceback) {
        for (int i = 0; i <= querySize; i++) {

            for (int j = 0; j <= subjectSize; j++) {

                printf("%c ", tracebackMatrix[i][j]);
            }
            printf("\n");
        }
    }else{

        for (int i = 0; i <= querySize; i++) {

            for (int j = 0; j <= subjectSize; j++) {

                printf("%d ", scoreMatrix[i][j]);
            }
            printf("\n");
        }
    }


}


