//
// Created by justin on 2/24/16.
//



#ifndef WUNCH_SCORINGMATRIX_H
#define WUNCH_SCORINGMATRIX_H

class ScoringMatrix {

public:

    ScoringMatrix();
    void setPenalties(int match, int mismatch, int gap);

    int** getMatrix();
    int getGap();

private:

    struct penalties{

        int match =0;
        int mismatch = 0;
        int gap =0;
    };

    penalties scores;
    int** simScoreMatrix;







};

#endif


