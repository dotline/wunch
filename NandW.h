//
//Justin Kincaid <JKincaid@nau.edu>
//Needleman and Wunsch algorithm
//uses a struct to store alignment results
//can be used to do Smith and Waterman Algorithm for alignment
//set doSandW to true to do a Smith and Waterman alignment
//
//
#ifndef WUNCH_NANDW_H
#define WUNCH_NANDW_H


#include <stddef.h>
#include <string>
#include <string.h>

#include <stdlib.h>

class NandW{

public:

    NandW();

    int NandWgetAlign(int** simScoreMatrix, int gapScore);
    void scoreAndTracebackInit(int gap);
    void setSubject(char *subject, int length);
    void setQuery(char *sequence, int length);
    void clearResults();
    void doS_W(bool=false);

    struct results{

        int score =0;
        char*subject;               //static subject to perform queries
        char*query;                 //query to perform on subject
        std::string alignSubject;   //alignment results string for subject
        std::string alignQuery;     //alignment results string for Query
        std::string traceback;      //alignment results traceback string

    };

    results getResults();
    void printMatrix(bool isTraceback);



private:





    results current;

    int** scoreMatrix;
    char** tracebackMatrix;
    int querySize;
    int subjectSize;
    char tbChar;
    char simScoreComp;
    int simScoreMatrix_x;
    int simScoreMatrix_y;
    int up;
    int left;
    int diagnal;
    int tempMax;
    int alignScore;
    enum traceBackChar { LEFT ='-', UP = '|', DIAG = '\\' , STOP = '0' };
    enum traceChar { MATCH ='|', MISMATCH = 'x', GAP = ' ' };
    bool doSandW;

    int max(int Up, int Diag, int Left, char* tbChar);






};


#endif //WUNCH_NANDW_H
