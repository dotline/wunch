//
//Justin Kincaid <JKincaid@nau.edu>
//builds a nucleotide similarity scoring matrix
//

#include "ScoringMatrix.h"

//defaults are set on initialization
ScoringMatrix::ScoringMatrix() {

    this->scores.match = 2;
    this->scores.mismatch = -1;
    this->scores.gap = -2;


}

//use to change the penalties
void ScoringMatrix::setPenalties(int match, int mismatch, int gap) {

    this->scores.match = match;
    this->scores.mismatch = mismatch;
    this->scores.gap = gap;

}


//returns the 2x2 scoring matrix
int**  ScoringMatrix::getMatrix() {



    int a = scores.match;
    int b = scores.mismatch;

    simScoreMatrix = new int*[4];

    simScoreMatrix[0] =  new int [4]{ a, b, b, b };
    simScoreMatrix[1] =  new int [4]{ b, a, b, b };
    simScoreMatrix[2] =  new int [4]{ b, b, a, b };
    simScoreMatrix[3] =  new int [4]{ b, b, b, a };




    return simScoreMatrix;
}

//get the gap score of the scoring matrix
int ScoringMatrix::getGap(){

    return this->scores.gap;
}
