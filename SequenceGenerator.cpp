//
//Created by Justin Kincaid <JKincaid@nau.edu> on 1/31/16.
//SequenceGenerator
//generates a random nucleotide query
//as an a array of char arrays
//


#include "SequenceGenerator.h"

using namespace std;
/*
 * Constructor
 * initialize a default query array
 * 1,000,000 sequences of 16-mers
 *
 */
SequenceGenerator::SequenceGenerator() {

    genes = new char [4] {'A','C','T','G'};
    containerArray = new char*[1000000];
    sequenceArray = new char [16];
    size = 1000000;
    mers = 16;
    returnArray = generateArray(this->size, this->mers);
}

/*
 * Constructor
 * initialize a query array to
 * @param size  # of sequences
 * @param mers  # of chars in query
 * query array is generated on initialization
 * and ready to return
 */
SequenceGenerator::SequenceGenerator(int size, int mers) {

    this->genes = new char [4] {'A','C','T','G'};
    this->containerArray = new char*[size];
    this->sequenceArray = new char [mers];

    this->returnArray = generateArray(size, mers);

}

/*
 * getArraySequence
 * returns the generated char* array of sequences
 */
char** SequenceGenerator::getArraySequence(){

    return this->returnArray;

}
/*
 * generateArray
 * query generator
 * generates char* array to
 * @param size  # of sequences
 * @param mers  # of chars in query
 * uses mersenne twister seeded once before generation
 * returns char* array of generated sequences
 */
char** SequenceGenerator::generateArray(int size, int mers) {

    mt19937_64 rng;
    rng.seed(random_device()());
    uniform_int_distribution<mt19937_64::result_type> distribution(0,3);

    for(int i = 0; i < size; i++) {

        for (int j = 0; j < mers; j++) {

            this->sequenceArray[j] = genes[distribution(rng)];

        }

        containerArray[i] =  this->sequenceArray;

        sequenceArray = new char[mers];
    }

    return containerArray;

}

bool SequenceGenerator::writeToFile(std::string filename,int size){

    ofstream file (filename);

    if(file.is_open()) {
        for (int i = 0; i < size; i++) {

            std::string tmp(returnArray[i]);
            file << tmp;
            file << "\n";

        }
         file.close();
        return true;
    }else
        return false;



}