//
// Justin Kincaid <JKincaid@nau.edu>
//Sequence Generator
//creates random nucleotide sequences/
//and stores them in a char** container
//


#ifndef WUNCH_SEQUENCEGENERATOR_H
#define WUNCH_SEQUENCEGENERATOR_H

#include <iostream>
#include <random>
#include <fstream>

class SequenceGenerator {

public:

    SequenceGenerator();
    SequenceGenerator(int size, int mers);
    char** getArraySequence();
    bool writeToFile(std::string filename,int size);

private:


    char** generateArray(int size, int mers);
    char** containerArray;
    char* sequenceArray;
    char* genes;
    char** returnArray;
    int size;
    int mers;



};



#endif //HASHTABLE_SEQUENCEGENERATOR_H
