

#include <iostream>
#include "string.h"
#include "SequenceGenerator.h"
#include "ScoringMatrix.h"
#include "NandW.h"

using namespace std;

int main() {


    int mersLength = 50;
    int numofSequences = 30;
    int modPrint = 100;
    bool doNandW = true;
    bool doSandW = false;
    char * testQ = new char[8] {'A','G','C','A','C','A','C','A'};
    char * testS = new char[8] {'A','C','A','C','A','C','T','A'};

    printf("GENERATING %d SEQUENCES OF LENGTH %d\n", numofSequences, mersLength);

    SequenceGenerator* sg = new SequenceGenerator(numofSequences,mersLength);
    NandW* nandw = new NandW();
    ScoringMatrix* sm = new ScoringMatrix();

    int ** gms = sm->getMatrix();

    sg->writeToFile("dengV2_random_50mer",numofSequences);

    //cout << gms [2][0] << endl;
/*
    char** sequences = sg->getArraySequence();
    char*subject = sequences[0];

    printf("SUBJECT STRING: \n%s\n", subject);
    char* sequence;

    int gap = sm->getGap();


    clock_t t;

    nandw->doS_W(true);

    nandw->setQuery(testQ,8);
    nandw->setSubject(testS,8);
    nandw->NandWgetAlign(gms,gap);

    NandW::results result;

    result = nandw->getResults();
    cout << endl;
    printf("Gap Score: %d\n", result.score);
    printf("subject :%s\n", result.alignSubject.c_str());
    printf("tracebk :%s\n", result.traceback.c_str());
    printf("query   :%s\n", result.alignQuery.c_str());



*//*
if(doNandW) {
    printf("USING NEEDLEMAN AND WUNSCH: \n");

    t = clock();
    clock_t r;
    for (int i = 1; i < numofSequences; i++) {

        r = clock();
        sequence = sequences[i];

        nandw->setSubject(subject, mersLength);

        nandw->setQuery(sequence, mersLength);
        //nandw->scoreAndTracebackInit(gap);

        nandw->NandWgetAlign(gms, gap);



        //if(i%20 == 0)
        //printf("%f \n", (float(clock()-r)/CLOCKS_PER_SEC));
        //else
        //printf("%f ", (float(clock()-r)/CLOCKS_PER_SEC));
        if (i % modPrint == 0) {

            printf("%f ", (float(clock() - t) / CLOCKS_PER_SEC));
            NandW::results result;

            result = nandw->getResults();
            cout << endl;
            printf("Gap Score: %d\n", result.score);
            printf("subject :%s\n", result.alignSubject.c_str());
            printf("tracebk :%s\n", result.traceback.c_str());
            printf("query   :%s\n", result.alignQuery.c_str());


        }


*//*
*//*

        NandW::results result;

        result = nandw->getResults();

        cout << "score : " << result.score << endl;
        nandw->printMatrix(1);
        cout << "ogquery: " << result.subject << endl;
        cout << "equery:  " << result.alignSubject << endl;
        cout << "tb       " << result.traceback << endl;
        cout << "seq      " << result.alignQuery << endl;
        cout << "ogseq    " << result.subject << endl;

        nandw->printMatrix(0);
        *//**//*



    }


    t = clock() - t;

    printf("\n%d sequences of %d-mers took %f seconds\n", numofSequences, mersLength, (float(t) / CLOCKS_PER_SEC));
}

if(doSandW) {
    printf("USING SMITH AND WATERMAN: \n");

    t = clock();
    nandw->doS_W(true);
    for (int i = 1; i < numofSequences; i++) {

        //r = clock();
        sequence = sequences[i];

        nandw->setSubject(subject, mersLength);

        nandw->setQuery(sequence, mersLength);
        //nandw->scoreAndTracebackInit(gap);

        nandw->NandWgetAlign(gms, gap);



        //if(i%20 == 0)
        //rintf("%f \n", (float(clock()-r)/CLOCKS_PER_SEC));
        //else
        //printf("%f ", (float(clock()-r)/CLOCKS_PER_SEC));
        if (i % modPrint == 0) {

            printf("%f ", (float(clock() - t) / CLOCKS_PER_SEC));
            NandW::results result;

            result = nandw->getResults();
            cout << endl;
            printf("Gap Score: %d\n", result.score);
            printf("subject :%s\n", result.alignSubject.c_str());
            printf("tracebk :%s\n", result.traceback.c_str());
            printf("query   :%s\n", result.alignQuery.c_str());


        }



        *//*
*//*
        NandW::results result;

        result = nandw->getResults();

        cout << "score : " << result.score << endl;
        nandw->printMatrix(1);
        cout << "ogquery: " << result.subject << endl;
        cout << "equery:  " << result.alignSubject << endl;
        cout << "tb       " << result.traceback << endl;
        cout << "seq      " << result.alignQuery << endl;
        cout << "ogseq    " << result.subject << endl;

        nandw->printMatrix(0);

        *//**//*

    }
    t = clock() - t;
    printf("\n%d sequences of %d-mers took %f seconds\n", numofSequences, mersLength, (float(t) / CLOCKS_PER_SEC));

}
*/




    return 0;



}